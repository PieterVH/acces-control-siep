/**
 * Title: A new IDP source file
 * Author: pietervh
 */
LTCvocabulary V{
	/*
	 * Types contain the complete domain of the problem.
	 * 		In this case being the principals, booleans and time
	 */
	type principal isa int
	type bool
	type actions
	type Time isa int
	Next(Time):Time
	Start:Time
	Me(principal)
	
	NODE1(Time,principal)
	NODE2(Time,principal)
	ACTION(Time,actions)
	new:actions
	wgd:actions
	wld:actions
	// For modeling this nice we need 3 constants, the source of authorization (SOA) and 2 booleans
	T:bool
	F:bool
	SOA:principal
	BOOL1(Time,bool)
	BOOL2(Time,bool)
	
	//link(i,j,T,T,1) says there exists a link between principal i and principal J which delegates access and delegation permission
	link(principal,principal,bool,bool,Time)
	linkstart(principal,principal,bool,bool)


	dlp(principal,Time) // principal has delegation permission on a
	alp(principal,Time) // principal has access permission on a
	
	//Transactions
	weakglobaldeleted(principal,principal,bool,bool,Time) //Weak Global Deleted
	weaklocaldeleted(principal,principal,bool,bool,Time) //Weak Local Deleted
	delegated(principal,principal,bool,bool,Time)
	
	//results of transactions
	deleted(principal,principal,bool,bool,Time) 
	newlymade(principal,principal,bool,bool,Time)
}
theory T:V{
	/*
	 * Define dlp
	 *  You have delegationpermission if
	 * 		- You are the source of authorization
	 * 		- there exists a link that gives delegationpermission from someone with delegationpermission 
	 */
	{
		dlp(p,t) <- p=SOA.
		dlp(p,t) <- ?p1:dlp(p1,t) & link(p1,p,T,T,t).
	}
	/*
	 * Define als
	 *  You have access permission if
	 * 		- You are the source of authorization
	 * 		- there exists a link that gives access permission from someone with delegationpermission 
	 */
	{
		alp(p,t) <- p=SOA.
		alp(p,t) <- ?p1:dlp(p1,t) & (?b[bool]:link(p1,p,T,b,t)).
	}	
	
	
	/*
	 * Defintion of deleted:
	 * 	A link should be deleted if
	 * 		- It is the one scheduled for deleted
	 * 		* Every link that gave permission to the startnode of this link is deleted
	 */
	{
		weakglobaldeleted(p1,p2,b1,b2,Next(t))<-link(p1,p2,b1,b2,t)&ACTION(t,wgd)&NODE1(t,p1)&NODE2(t,p2)&Me(p1).
	}
	{
		weaklocaldeleted(p1,p2,b1,b2,Next(t))<-link(p1,p2,b1,b2,t)&ACTION(t,wld)&NODE1(t,p1)&NODE2(t,p2)&Me(p1).
	}
	
	{
		deleted(p1,p2,b1,b2,t)<- weakglobaldeleted(p1,p2,b1,b2,t).
		deleted(p1,p2,b1,b2,t)<- weaklocaldeleted(p1,p2,b1,b2,t).
		deleted(p2,p3,b1,b2,Next(t))<-link(p2,p3,b1,b2,t)&
			(?p1 b3 b4: link(p1,p2,b3,b4,t))
			&
			(!p1 b3 b4:(link(p1,p2,b3,b4,t)=>weaklocaldeleted(p1,p2,b3,b4,Next(t)))
		).//propagation of deleted 	
		deleted(p2,p3,b1,b2,Next(t))<-link(p2,p3,b1,b2,t)&
			(?p1 b3 b4: link(p1,p2,b3,b4,t))
			&
			(!p1 b3 b4:(link(p1,p2,b3,b4,t)=>deleted(p1,p2,b3,b4,Next(t)))
		).//propagation of deleted 	(notice: stronger than with wld)		
	}
	{
		delegated(p1,p2,b1,b2,Next(t))<- ~link(p1,p2,b1,b2,t)&ACTION(t,new)&NODE1(t,p1)&NODE2(t,p2)&BOOL1(t,b1)&BOOL2(t,b2)&Me(p1)&dlp(p1,t).
	}
	
	
	{
		newlymade(p1,p2,b1,b2,t)<-delegated(p1,p2,b1,b2,t).
		newlymade(start,stop,b1,b2,Next(t)) <- ?n:weaklocaldeleted(start,n,b3,b4,Next(t))&link(n,stop,b1,b2,t).
	}

	{
		link(p1, p2, b1, b2, Start)<-linkstart(p1, p2, b1, b2).
		link(p1, p2, b1, b2, Next(t))<-link(p1, p2, b1, b2, t)&~deleted(p1, p2, b1, b2, Next(t)).
		link(p1, p2, b1, b2, t)<-newlymade(p1, p2, b1, b2, t).
	}
	
	//Permissions
	
	//!t[Time]:! p[principal]:NODE1(t, p)=>p=Me.
	
	//Cheating
	!t[Time]:?=1 a[actions]:ACTION(t, a).
	!t[Time]:?=1 p[principal]:NODE1(t, p).
	!t[Time]:?=1 p[principal]:NODE2(t, p).
	!t[Time]:?=1 b[bool]:BOOL1(t, b).
	!t[Time]:?=1 b[bool]:BOOL2(t, b).
}

structure S1:V{
	Time={0..3}
	principal={1..6}
	SOA=1
	bool={T;F}
	T=T
	F=F
	linkstart={(1,2,T,T);(1,4,T,T);(4,2,T,F);(2,5,T,T);(2,3,T,F);(4,5,T,T);(5,6,F,F)}
	actions={wgd;new;wld}
	//Wgd={(1,2,T,T,1)}
	new=new
	wgd=wgd
	wld=wld
}


include <mx>
include "database.idp"
procedure main(){
	//Options
		stdoptions.xsb=false
		stdoptions.groundwithbounds=false
		stdoptions.liftedunitpropagation=false
		stdoptions.nbmodels=1
		merge(S1,S2)
						//	//print(S1)
	local next
	if (S~=nil) then
		//Progression
			print("Doing Progression")
			//Print
				//print("Structure:")
				print("     Link: ", " ",S[V_ss::link].ct)
//				print("")
//				print("     Deleted: ","", S[V_ss::deleted].ct)
//				print("     New: ", " ", S[V_ss::newlymade].ct)
//				print("")
//				print("     GlobalDeleted: ", S[V_ss::weakglobaldeleted].ct)
//				print("     LocalDeleted: ", S[V_ss::weaklocaldeleted].ct)
//				print("     Delegated: ", S[V_ss::delegated].ct)		
				//print(S)
			//Get Input
				print("Who are you? Choose from [1..6]") //TODO MODIFY MESSAGE TO ASK FOR ACTION
				inp0 = io.read("*line")
				print("Action? Choose from [wld,wgd,new]") //TODO MODIFY MESSAGE TO ASK FOR ACTION
				inp1 = io.read("*line")
				if(inp1=="new")	then
					print("Ok, new delegation. Access level permission? [T,F]")
					inp4= io.read("*line")
					print("Delegation level permission? [T,F]")	
					inp5=io.read("*line")
					S[V_ss::BOOL1].ct = {}
					S[V_ss::BOOL1].cf = {}
					maketrue(S[V_ss::BOOL1],{inp4})	
					S[V_ss::BOOL1].pt = S[V_ss::BOOL1].ct
					
					S[V_ss::BOOL2].ct = {}
					S[V_ss::BOOL2].cf = {}
					maketrue(S[V_ss::BOOL2],{inp5})	
					S[V_ss::BOOL2].pt = S[V_ss::BOOL2].ct
				end		
				print("StartNode?")
				inp2 = tonumber(io.read("*line")) //TODO READ NODE1

				print("EndNode?") 
				inp3 = tonumber(io.read("*line")) //TODO READ NODE2
				
				S[V_ss::Me].ct = {}
				S[V_ss::Me].cf = {}
				maketrue(S[V_ss::Me],{inp0})	
				S[V_ss::Me].pt = S[V_ss::Me].ct
					
				S[V_ss::ACTION].ct = {}
				S[V_ss::ACTION].cf = {}
				maketrue(S[V_ss::ACTION],{inp1})	
				S[V_ss::ACTION].pt = S[V_ss::ACTION].ct
				
				S[V_ss::NODE1].ct = {}
				S[V_ss::NODE1].cf = {}
				maketrue(S[V_ss::NODE1],{inp2})	
				S[V_ss::NODE1].pt = S[V_ss::NODE1].ct
		
				S[V_ss::NODE2].ct = {}
				S[V_ss::NODE2].cf = {}
				maketrue(S[V_ss::NODE2],{inp3})	
				S[V_ss::NODE2].pt = S[V_ss::NODE2].ct		
			
			next = progress(T,S)[1]
	else
		//INITIALISE
			print("Welcome. Initializing...")
			curStructure = calculatedefinitions(T,S1) 
			next = initialise(T,curStructure)[1]
	end
	//Print
		print("Structure:")
		print("     Link: ", " ",next[V_ss::link].ct)
		print("")
		print("     Deleted: ","", next[V_ss::deleted].ct)
		print("     New: ", " ", next[V_ss::newlymade].ct)
		print("")
		print("     GlobalDeleted: ", next[V_ss::weakglobaldeleted].ct)
		print("     LocalDeleted: ", next[V_ss::weaklocaldeleted].ct)
		print("     Delegated: ", next[V_ss::delegated].ct)				
	//Update database					
	setname(next, "S")

	
	file = io.open( "database.idp", "w")
 	file:write("//Structure generated by database app\n")
 	file:write(idpintern.tostring(next))
	file:close()					

//	possStrucs,a,b,initVoc =initialise(T,curStructure)
//	print("welcome")
//	
//	struc=possStrucs[1]
	

		
}
 